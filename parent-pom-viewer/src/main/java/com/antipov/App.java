package com.antipov;

import java.io.IOException;

public class App 
{
    private static final String PARENT_PROJECT_PATH = "/Users/eugeneantipov/Documents/selfEducation/homework/mvn-homework/parent-project";

    public static void main(String[] args ) throws IOException {
        PomViewer.printPom(PARENT_PROJECT_PATH);
    }
}
