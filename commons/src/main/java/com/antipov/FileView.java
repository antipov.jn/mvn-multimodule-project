package com.antipov;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

class FileView {
public void showFile(String fileName) throws IOException {
    File file = new File(fileName);
        System.out.println(FileUtils.readFileToString(file, StandardCharsets.UTF_8));

}
}