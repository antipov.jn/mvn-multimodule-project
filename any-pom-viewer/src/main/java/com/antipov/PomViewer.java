package com.antipov;

import java.io.IOException;

public class PomViewer {

    public static void printPom(String absolutePathToProject) throws IOException {
        String pomPath = absolutePathToProject + "/pom.xml";
        new FileView().showFile(pomPath);
    }
}
