package com.antipov;

import java.io.IOException;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        System.out.println("Please enter absolute path to the project:");
        Scanner scanner = new Scanner(System.in);
        PomViewer.printPom(scanner.nextLine());
    }
}
